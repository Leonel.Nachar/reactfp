import { initializeApp } from "firebase/app";
import { getFirestore } from "firebase/firestore"


const firebaseConfig = {
    apiKey: "AIzaSyA4ixcmKcMynQltZFltnPYqB3BWiugJdVA",
    authDomain: "reactfp-29534.firebaseapp.com",
    projectId: "reactfp-29534",
    storageBucket: "reactfp-29534.appspot.com",
    messagingSenderId: "691534736340",
    appId: "1:691534736340:web:deb16a6bb69d4f01d6c441"
};


const app = initializeApp(firebaseConfig);

export const db = getFirestore(app);