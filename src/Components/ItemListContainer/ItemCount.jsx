import "./ItemCont.css"
import { useState } from "react";


function Buy ({stock, initial, onAdd}) {

    const [count, setCount] = useState(0);

    const añadir = () =>{
        if (count < stock) {
            setCount(count + 1)
        }
    }

    const quitar = () =>{
        if (count > initial) {
            setCount(count - 1)
        }
    }

    const handlerClickAdd = () =>{
        onAdd(count)
    }

    return (
        <>
            <div className="btn-full">
                <div className="itemContador">
                <button onClick={quitar} className="btn-quitar">
                    -
                </button>
                <p>{ count }</p>
                <button onClick={añadir} className="btn-añadir">
                    + 
                </button>
                </div>
                
                <div>
                    <button onClick={handlerClickAdd} className="btnBuy">
                        Comprar ahora
                    </button>
                </div>
            </div>
            
        </>
    )
}

export default Buy