import React, {useState, useEffect} from "react";
import "./ItemCont.css"
import ItemList from "../ItemList/ItemList";
import { useParams } from "react-router-dom";
import { db } from "../../firebase/firebase";
import { getDocs, collection, query, where } from "firebase/firestore";


function List()  {
    const [data,setData] = useState([]);

    const {categoryName} = useParams();
    
    useEffect(() => {
        const productoCollection = collection(db, 'productos');
        const q = query(productoCollection, where("category", "==", `${categoryName}`));



        getDocs(categoryName ? q : productoCollection)
        .then(result =>{
            const lista = result.docs.map(doc =>{
                return{
                    id: doc.id,
                    ...doc.data(),
                }
            })
            setData(lista);
        })
        .catch(err => console.log(err));
    }, [categoryName]);
    

    return (
        <div>
            <div className="productosNw">
                <ItemList  data={data} />
            </div>
        </div>
    )
}

export default List