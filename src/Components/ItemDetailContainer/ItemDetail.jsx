import "./details.css"
import Buy from "../ItemListContainer/ItemCount";
import { useState } from "react";
import { Link } from "react-router-dom";
import { useContext } from "react";
import { contexto } from "../../Context/CartContext";

const ItemDetail = ({productos}) => {

    const [finalized, setFinalized] = useState(false)
    const { addProductos } = useContext(contexto);

    const onAdd = (count) =>{
        setFinalized(true);
        addProductos({...productos, qty: count});
        
    }

    return(
        <div>
            <hr />
            <h1>{productos.title}</h1>
            <div className="containerAl">
            
            <div className="imgCel">
            <img src={productos.image} alt="" />
            </div>
            <div className="infoS">
                <ul>
                    <li>{productos.inf}</li>
                </ul>
            </div>
            <div className="caractC">
                <h2>Características importantes a saber:</h2>
                    <ul>
                        <li>{productos.caract}</li>
                    </ul>
            </div>
            
            <div className="descripC">
                <p>Descripción:</p>
                <p>{productos.descripcion}</p>
                <p>Precio:</p>
                <p>{productos.price}AR$</p>
            </div>
            </div>
            {!finalized ? <Buy initial={1} stock={5} onAdd={onAdd} /> : <Link to="/cart"><button className="btn-finish">Terminar la compra</button></Link>}


            <hr />
        </div>
    );
};

export default ItemDetail;