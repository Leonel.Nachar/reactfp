import React, {useState, useEffect} from "react";
import { useParams } from "react-router-dom";
import ItemDetail from "./ItemDetail";
import { db } from "../../firebase/firebase";
import { collection, doc, getDoc } from "firebase/firestore";



const ItemDetailContainer = () => {
    const [detalles,setDetalles] = useState([]);
    const { detalleId } = useParams();

    useEffect(() => {
            const productoCollection = collection(db,'productos');
            const obtDoc = doc(productoCollection, detalleId);
            getDoc(obtDoc)
            .then(result =>{
                const producto = {
                    id: result.id,
                    ...result.data(),
                }
                setDetalles(producto);
            })
            .catch(err => console.log(err));
    }, [detalleId]);

    return (
        <div>
            <ItemDetail productos={detalles} />
        </div>
    )
}

export default ItemDetailContainer