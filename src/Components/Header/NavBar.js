import React from "react"
import { Link } from "react-router-dom"
import logo from '../../assets/imagenlogo.jpg'
import Carro from "../Cart/CartWidget"

import './Header.css'

const NavBar = () => {


    const categories = [
        { name: "motorola", id:0, route:"/category/motorola"},
        { name: "samsung", id:1, route:"/category/samsung"},
        { name: "iphone", id:2, route:"/category/iphone"},
    ]


    return (
        <header>
            <div>
                <Link to="/"> <img src={logo} alt="logo"/> </Link>
            </div>
            
            
            <nav>
                
                {categories.map((category) => <Link key={category.id} to={category.route}>{category.name}</Link>)}
                
            </nav>
            <Carro />
            

        </header>
    )
}
export default NavBar