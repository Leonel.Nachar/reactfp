import React from 'react';
import { Link } from 'react-router-dom';
import item from './item.css'

const Item = ({info}) => {
    return(
        
        <div className='CardsDiv'>
            <Link to={`/category/${info.category}/${info.id}`}>
            <div className="card">
                <div className="card-img"><img src={info.image} alt="" /></div>
                <div className="card-info">
                    <p className="text-title">{info.title}</p>
                    <p className="text-body">{info.descripcion}</p>
                </div>
                <div className="card-footer">
                <span className="text-title">{info.price}</span>
                </div>
            </div>
            </Link>
        </div>
        
    );
}

export default Item;