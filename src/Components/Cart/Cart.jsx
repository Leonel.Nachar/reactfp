import { useContext } from "react"
import { Link } from "react-router-dom";
import { contexto } from "../../Context/CartContext"
import { MdDeleteForever } from "react-icons/md"
import "./cart.css"

const Cart = () => {
    
    const {productos} = useContext(contexto);
    const {clear} = useContext(contexto);
    const {delProducto} = useContext(contexto);
    const {qtyProductos} = useContext(contexto);
    const priceTotal = productos.reduce((acc, el) => acc + (el.price * el.qty), 0)

    return(

        

        <>
        
            {qtyProductos === 0
            ?
            <div>
                <div className="noProductsCart">
                    <h1>No hay productos en el carrito</h1>
                    <div className="addBtn">
                        <h2 className="addBtn-c"><Link to="/" >Click acá para añadir</Link></h2>
                    </div>
                    
                </div>
            </div>
            : 
            <div>
            <h1>Acá estan sus productos</h1>
            
                <div>
                    {productos.map(p => 
                        <div>
                            <div className="cartProducts">
                                <h1 className="cartProductsName">{p.title}</h1>
                                <img src={p.image} alt={p.title}/>
                                <p>Cantidad: {p.qty}</p>
                                <p>Precio:{p.price * (p.qty)}AR$</p>
                            



                                <button className="btnDelProd"  onClick={() => delProducto(p.id)}> <span className="text">Eliminar producto</span> <span><MdDeleteForever style={{width:20 , height:20}} className="mdDelete" /></span> </button>
                            </div>
                        </div>
                        
                        )}
                    
                    <div className="cartProductsFin">

                        <h2>Precio total: {priceTotal} AR$</h2>
                        <button className="btnDelProd" onClick={clear}><span className="text">Eliminar todos los productos</span></button>
                        <button className="btnContinProd"><Link to="/" >Seguir comprando</Link></button>
                        <button className="btnContinProd"><Link to="/cartTerminado">Finalizar compra</Link></button>
                    </div>
                    
                </div>
            </div>
            }
        
        
        
        
        </>
        
        
        
        
    )
};



export default Cart