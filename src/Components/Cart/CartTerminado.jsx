import { db } from "../../firebase/firebase";
import { collection, addDoc, serverTimestamp } from "firebase/firestore";
import { contexto } from '../../Context/CartContext';
import { useContext } from "react";
import { useState } from "react";
import { Link } from "react-router-dom";


    const CartTerminado = () => {
        const { productos } = useContext(contexto);
        const { qtyProductos } = useContext(contexto);
        const [ nombre, setNombre ] = useState('');
        const [ numero, setNumero ] = useState('');
        const [ email, setEmail ] = useState('');
        const [ idVenta, setIdVenta ] = useState([]);
        const priceTotal = productos.reduce((acc, el) => acc + (el.price * el.qty), 0)

        const finalizarCompra = () => {
            const ventasCollection = collection(db, 'ventas');
            addDoc(ventasCollection, {

                name: nombre,
                telefono: numero,
                mail: email,
                items: productos,
                date: serverTimestamp(),
                cantidad: qtyProductos,
                total: priceTotal,

            })
            .then((result) =>{
                setIdVenta(result.id)
            })
        }
        

    return(
        <>
            {idVenta.length === 0
            ?
            <div className="cartFinContainer">
                <h2>Rellene el formulario:</h2>
                <label> Nombre </label>
                <input type="text" onChange={e => setNombre(e.target.value)} />
                <label> Teléfono </label>
                <input  type="text" onChange={e => setNumero(e.target.value)} />
                <label> Email </label>
                <input type="text" onChange={e => setEmail(e.target.value)} />
                <button className="btn-enviar" onClick={finalizarCompra}> Enviar </button>
            </div>
            :
            <div className="cartFinContainer">
                <h2> Haz completado tu compra, felicidades! </h2>
                <p> Tu id de compra: {idVenta}</p>
                <p> Desea seguir comprando? <Link to="/"><button>Click aquí</button></Link></p>
            </div>
        }
        </>
        



    )

    }

    export default CartTerminado