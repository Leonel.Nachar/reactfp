import { useContext } from 'react'
import {BsCart2} from 'react-icons/bs'
import { contexto } from '../../Context/CartContext'
import { Link } from "react-router-dom";


const Carro = () =>{

    const { qtyProductos } = useContext(contexto);
    const { productos } = useContext(contexto)


    return(
        <div style={{display: 'flex'}}>

                <Link to="/cart">
                    <BsCart2 style={{marginTop: 30}}/>
                </Link>
                
            

        {productos.length === 0 ? <p></p> : <p>{qtyProductos}</p>}
        </div>
        
    )
}

export default Carro