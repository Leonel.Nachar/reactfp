import NavBar from './Components/Header/NavBar'
import './App.css';
import List from './Components/ItemListContainer/ItemListContainer';
import ItemDetailContainer from './Components/ItemDetailContainer/ItemDetailContainer';
import Cart from './Components/Cart/Cart.jsx'
import CartContext from './Context/CartContext'
import { 
    BrowserRouter,
    Routes,
    Route,
  } from 'react-router-dom';
import CartTerminado from './Components/Cart/CartTerminado';

const App = () => {
  return (
    <>
      <BrowserRouter>
        <CartContext>
          <NavBar />
            <Routes>
              <Route path='/' element={<List />}></Route>
      
              <Route path='/category/:categoryName' element={<List />}></Route>

              <Route path="/category/:categoryName/:detalleId" element={<ItemDetailContainer />}></Route>
    
              <Route path='/cart' element={<Cart />} />

              <Route path='/cartTerminado' element={<CartTerminado />}/>
            </Routes>
        </CartContext>
      </BrowserRouter>
    </>
  );
}

export default App;
