import React, { useState , useEffect, createContext } from "react";


export const contexto = createContext();
const { Provider } = contexto;


const CartContext = ({children}) => {

    const [productos, setProductos] = useState([]);
    const [qtyProductos, setQtyProductos] = useState(0);
    
    const getProductosQty = () => {
        let qty = 0;
        productos.forEach(producto => {
            qty += producto.qty;
        })
        setQtyProductos(qty);
    }

    useEffect(() => {
        getProductosQty();
    },);

    function addProductos(producto) {
        if (IsInCart(producto.id)){
        const productoFind = productos.find(p => p.id === producto.id);
        const index = productos.indexOf(productoFind);
        const aac = [...productos];
        aac[index].qty += producto.qty;
        setProductos(aac);
        } else {
            setProductos([...productos,{...producto}]);
        };
    }

    const delProducto = (id) => {
        setProductos(productos.filter(productos => productos.id !== id));
    }

    const IsInCart = (id) => {
        return productos.some(productos => productos.id === id)
    };

    const clear = () => {
        setProductos([]);
        setQtyProductos(0);
    }

    return (
        <Provider value={{ productos, addProductos, qtyProductos, delProducto, clear, IsInCart }}>
            {children}
        </Provider>

        
    )
}

export default CartContext